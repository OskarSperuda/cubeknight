﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floor : MonoBehaviour
{
    
    [SerializeField] private Color visitedColor = Color.blue;
    //public float Speed = 1, Offset;

    private new Renderer _renderer;
    private MaterialPropertyBlock _propBlock;
    private static readonly int BaseColor = Shader.PropertyToID("_BaseColor");

    private void Awake()
    {
        _propBlock = new MaterialPropertyBlock();
        _renderer = GetComponentInChildren<Renderer>();
    }

    public void SetVisited()
    {
        
            _renderer.GetPropertyBlock(_propBlock);
            _propBlock.SetColor(BaseColor, visitedColor);
            _renderer.SetPropertyBlock(_propBlock);
        
    }

}
