﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    
    [SerializeField] private Vector2 playerCoords = new Vector2(2,0);
    [SerializeField] private GameObject wallPrefab, floorPrefab;
    [SerializeField] private Transform playerTransform;
    [SerializeField] private float cubeSize = 1, speed = 10;
    private static int _unvisitedFloors = 0;
    private Vector3 _playerWorldPos;
    private bool _isMoving = false;
    private static int[][] mapArr = new int[][] 
    {
        new int[] {1, 1, 1, 1, 1, 1},
        new int[] {1, 0, 0, 0, 0, 1},
        new int[] {1, 0, 1, 1, 1, 1},
        new int[] {1, 0, 0, 0, 1, 1},
        new int[] {1, 0, 0, 1, 1, 1},
        new int[] {1, 1, 0, 0, 0, 1},
        new int[] {1, 1, 1, 1, 1, 1}
    };

    private static Floor[][] _floors = new Floor[][]
    {
        new Floor[] {null, null, null, null, null, null},
        new Floor[] {null, null, null, null, null, null},
        new Floor[] {null, null, null, null, null, null},
        new Floor[] {null, null, null, null, null, null},
        new Floor[] {null, null, null, null, null, null},
        new Floor[] {null, null, null, null, null, null},
        new Floor[] {null, null, null, null, null, null}
    };

    private void Start()
    {
        BuildMap();
        
        _playerWorldPos = CoordsToPosition((int)playerCoords.x,(int)playerCoords.y);
        MarkVisited((int)playerCoords.y,(int)playerCoords.x);
        playerTransform.position = _playerWorldPos;
        //SetPlayerPosition();

        SwipeDetector.OnSwipe += HandleInput;
    }

    private void OnDestroy()
    {
        SwipeDetector.OnSwipe -= HandleInput;
    }

    private void HandleInput(Direction dir)
    {
        if (_isMoving)
            return;
        
        playerCoords = MoveTo(playerCoords, dir);
        SetPlayerPosition();
    }
    private void Update()
    {
        if (playerTransform.position != _playerWorldPos)
        {
            _isMoving = true;
            float step = speed * Time.deltaTime;
            playerTransform.position = Vector3.MoveTowards(playerTransform.position, _playerWorldPos, step);
        }
        else
        {
            _isMoving = false;
        }

        /*
        if (_isMoving)
            return;
        
        if (Input.GetKeyDown("down"))
        {
            playerCoords = MoveTo(playerCoords, Direction.Down);
            SetPlayerPosition();
        }
        else if (Input.GetKeyDown("up"))
        {
            playerCoords = MoveTo(playerCoords, Direction.Up);
            SetPlayerPosition();
        }
        else if (Input.GetKeyDown("right"))
        {
            playerCoords = MoveTo(playerCoords, Direction.Right);
            SetPlayerPosition();
        }
        else if (Input.GetKeyDown("left"))
        {
            playerCoords = MoveTo(playerCoords, Direction.Left);
            SetPlayerPosition();            
        }
        */
    }

    private void SetPlayerPosition()
    {
        _playerWorldPos = CoordsToPosition((int)playerCoords.x, (int)playerCoords.y);
        //playerTransform.position = playerWorldPos;
    }

    private static Vector2 MoveTo(Vector2 startPos, Direction dir)
    {
        var currX = (int)startPos.x;
        var currY = (int)startPos.y;
        
        if (mapArr[currY][currX] == 1)
        {
            Debug.Log("started at invalid position");
            return startPos;
        }

        switch (dir)
        {
            case Direction.Down:    //moving down
            {
                while (currY+1<mapArr.Length)
                {
                
                    if (mapArr[currY+1][currX] != 1)
                    {
                        currY++;
                        MarkVisited(currY, currX);
                    }
                    else
                    {
                        return new Vector2(currX,currY);
                    }
                }

                break;
            }
            case Direction.Up:    //moving up
            {
                while (currY-1>=0)
                {
                
                    if (mapArr[currY-1][currX] != 1)
                    {
                        currY--;
                        MarkVisited(currY, currX);
                    }
                    else
                    {
                        return new Vector2(currX,currY);
                    }
                }

                break;
            }
            case Direction.Right:    //moving right
            {
                while (currX+1<mapArr[currY].Length)
                {
                
                    if (mapArr[currY][currX+1] != 1)
                    {
                        currX++;
                        MarkVisited(currY, currX);
                    }
                    else
                    {
                        return new Vector2(currX,currY);
                    }
                }

                break;
            }
            case Direction.Left:    //moving left
            {
                while (currX-1>=0)
                {
                
                    if (mapArr[currY][currX-1] != 1)
                    {
                        currX--;
                        MarkVisited(currY, currX);
                    }
                    else
                    {
                        return new Vector2(currX,currY);
                    }
                }

                break;
            }
            default:
            {
                Debug.Log("wrong direction");
                return startPos;
            }
                
        }

        return new Vector2(currX,currY);
    }

    private static void MarkVisited(int k, int n)
    {
        if (mapArr[k][n] == 0 )
        {
            mapArr[k][n] = 2;
            _floors[k][n].SetVisited();
            _unvisitedFloors--;
            if (_unvisitedFloors <= 0)
            {
                Win();
            }
        }
        
    }
    
    
    private void BuildMap()
    {
        for (int n = 0; n < mapArr.Length; n++)
        {
            for (int k = 0; k < mapArr[n].Length; k++)
            {
                if (mapArr[n][k] == 1)    //create walll
                {
                    Instantiate(wallPrefab, CoordsToPosition(k,n), Quaternion.identity, transform );
                }
                else if (mapArr[n][k] == 0) //create floor
                {
                    GameObject obj = Instantiate(floorPrefab, CoordsToPosition(k,n), Quaternion.identity, transform );
                    _floors[n][k] = obj.GetComponent<Floor>();
                    _unvisitedFloors++;
                }
            }
        }
    }

    private Vector3 CoordsToPosition(int k, int n)
    {
        float x = k * cubeSize;
        float z = n * -cubeSize;
        
        return new Vector3(x,0, z);
    }

    private static void Win()
    {
        Debug.Log("You won!");
    }
    
}

public enum Direction {Up, Down, Right, Left};
